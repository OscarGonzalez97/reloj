/*let a = 5
let b = 4
let cadena = "sadf"
let cadena2 = "sae3"
var listaDeNombres3: Array<String> = []

//También se declarar así:

var listaDeNombres: [String] = []
var listaDeNombres1 = [String]()
var listaDeNombres2 = Array<String>()
print(cadena + cadena2)

//var a: String = "5"

//var b: Int = Int(a)
let myString1 = "556"
let myInt1 = Int(myString1)
print(myInt1)

let a = readLine()
let myInt2 = Int(a) ?? 0
print(myInt2)

if !false || true {
	print("algo cierto")
}*/
var value = readLine()
if let value {
	let t = type(of: value)    
	print("'\(value)' of type '\(t)'")
	let myInt2 = Int(value)!
	let myInt = Int(value) ?? 0
	print(myInt)
} else {
    print("nulo")
}
//let t = type(of: value)
//    print("'\(value)' of type '\(t)'")
let str = "Foundation Swift String”

for char in str {
    print("character = \(char)")
}
